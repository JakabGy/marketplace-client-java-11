package com.atlassian.marketplace.client.http;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * Allows the caller to attach custom request headers to all client requests.
 * @since 2.0.0
 */
public interface RequestDecorator
{
    /**
     * Returns a map of header names and values that should be added to outgoing
     * Marketplace requests.
     */
    Map<String, String> getRequestHeaders();
    
    abstract class Instances
    {
        /**
         * Returns a {@link RequestDecorator} instance that always adds the same constant header values.
         * @param headers  a map of header names and values
         */
        public static RequestDecorator forHeaders(Map<String, String> headers)
        {
            return () -> ImmutableMap.copyOf(headers);
        }
    }
}
