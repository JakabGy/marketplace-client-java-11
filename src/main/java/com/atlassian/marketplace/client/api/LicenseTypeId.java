package com.atlassian.marketplace.client.api;

import java.net.URI;

/**
 * Simple value wrapper for a resource URI when it is used to identify a license type.
 * @since 2.0.0
 */
public final class LicenseTypeId extends ResourceId
{
    private LicenseTypeId(URI uri)
    {
        super(uri);
    }
    
    public static LicenseTypeId fromUri(URI uri)
    {
        return new LicenseTypeId(uri);
    }
}