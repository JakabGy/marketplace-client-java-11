package com.atlassian.marketplace.client.api;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.AddonCategorySummary;

/**
 * Starting point for all resources that return add-on category information.
 * @since 2.0.0
 */
public interface AddonCategories
{
    /**
     * Returns the list of all categories that are relevant to the specified application.
     * @param appKey an {@link ApplicationKey}
     * @return the list of categories
     * @throws MpacException  if the server was unavailable or returned an error
     */
    Iterable<AddonCategorySummary> findForApplication(ApplicationKey appKey) throws MpacException;
}
