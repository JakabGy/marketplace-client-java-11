package com.atlassian.marketplace.client.encoding;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Indicates that an object returned by the server (or constructed by the caller) was invalid
 * because a required field was omitted.
 * @since 2.0.0
 */
public class MissingRequiredField extends SchemaViolation
{
    private final String name;
    
    public MissingRequiredField(Class<?> schemaClass, String name)
    {
        super(schemaClass);
        this.name = checkNotNull(name);
    }
    
    /**
     * The name of the field that was not found.
     */
    public String getName()
    {
        return name;
    }
    
    @Override
    public String getMessage()
    {
        return "missing required property \"" + name + "\" in " + getSchemaClass().getSimpleName();
    }
}
