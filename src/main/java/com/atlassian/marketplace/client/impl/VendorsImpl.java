package com.atlassian.marketplace.client.impl;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.api.VendorQuery;
import com.atlassian.marketplace.client.api.Vendors;
import com.atlassian.marketplace.client.model.Vendor;
import com.atlassian.marketplace.client.model.VendorSummary;
import com.atlassian.marketplace.client.util.UriBuilder;

import java.util.Optional;

final class VendorsImpl extends ApiImplBase implements Vendors
{
    VendorsImpl(ApiHelper apiHelper, InternalModel.MinimalLinks root) throws MpacException
    {
        super(apiHelper, root, "vendors");
    }

    @Override
    public Optional<Vendor> safeGetById(VendorId id) throws MpacException
    {
        return apiHelper.safeGetOptionalEntity(id.getUri(), Vendor.class);
    }

    @Override
    public Vendor createVendor(Vendor vendor) throws MpacException
    {
        return genericCreate(getApiRoot(), vendor, Optional.empty());
    }
    
    @Override
    public Vendor updateVendor(Vendor original, Vendor updated) throws MpacException
    {
        return genericUpdate(original.getSelfUri(), original, updated);
    }

    @Override
    public Page<VendorSummary> find(VendorQuery query) throws MpacException
    {
        UriBuilder uri = fromApiRoot();
        ApiHelper.addVendorQueryParams(query, uri);
        return apiHelper.getMore(new PageReference<>(
            uri.build(), query.getBounds(), pageReader(InternalModel.Vendors.class)));
    }
}
