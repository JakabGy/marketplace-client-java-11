package com.atlassian.marketplace.client.api;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;

public class ApplicationKeyTest
{
    @Test(expected=NullPointerException.class)
    public void cannotConstructWithNullKey()
    {
        ApplicationKey.valueOf(null);
    }
    
    @Test(expected=NullPointerException.class)
    public void cannotConstructWithEmptyString()
    {
        ApplicationKey.valueOf("");
    }
    
    @Test(expected=NullPointerException.class)
    public void cannotConstructWithWhitespaceString()
    {
        ApplicationKey.valueOf(" \t ");
    }
    
    @Test
    public void keyContainsString()
    {
        assertThat(ApplicationKey.valueOf("key").getKey(), equalTo("key"));
    }
    
    @Test
    public void equivalentKeysAreEqual()
    {
        assertThat(ApplicationKey.valueOf("key"), equalTo(ApplicationKey.valueOf("key")));
    }
    
    @Test
    public void nonEquivalentKeysAreNotEqual()
    {
        assertThat(ApplicationKey.valueOf("key"), not(equalTo(ApplicationKey.valueOf("other"))));
    }
    
    @Test
    public void equivalentKeysHaveSameHashCode()
    {
        assertThat(ApplicationKey.valueOf("key").hashCode(), equalTo(ApplicationKey.valueOf("key").hashCode()));
    }
    
    @Test
    public void predefinedKeyReusesStaticInstanceCaseInsensitively()
    {
        assertThat(ApplicationKey.valueOf("Bamboo"), sameInstance(ApplicationKey.BAMBOO));
    }
}
